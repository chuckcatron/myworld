// JavaScript Document
var WorkItem = kendo.data.Model.define({
		id: "objectId",
		fields: {
	        company: {},
            end_date: { },
            start_date:{ },
            job_description: {},
            sort: { type: 'number'}
	    }
	});

var EducationItem = kendo.data.Model.define({
		id: "objectId",
		fields: {
	        college_name: {},
            graduation_date: { },
            degree_type:{ },
            degree_title: {},
            sort: { type: 'number'}
	    }
	});

var baseUrl = "https://api.parse.com/1/classes/"
var headers = { "X-Parse-Application-Id": "vtW9RsW8tc3eWXCqM49rQkQRVKzaONyLa74GZUlR", "X-Parse-REST-API-Key": "1uEm9H2EdROQS4Z5TKNHIuEt6ZjVRZyqtxExNIhV" };
var listView;
var WorkDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + 'WorkHistory',
                    headers: headers,
                    dataType: "json"
                }        
            },
            sort: { field: "sort", dir: "asc" },
            schema: {
                model: WorkItem,
                data: "results"
            },
            error: function(e) {
                    console.log(e);
            }
        });

var EducationDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + 'Education',
                    headers: headers,
                    dataType: "json"
                }        
            },
            sort: { field: "sort", dir: "asc" },
            schema: {
                model: EducationItem,
                data: "results"
            },
            error: function(e) {
                    console.log(e);
            }
        });

// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {
    navigator.splashscreen.hide();
}

function getMap() {
    // Use Google API to get a map of the current location
    var googleApis_map_Url = 'http://maps.googleapis.com/maps/api/staticmap?size=280x300&maptype=hybrid&zoom=6&markers=size:mid%7Ccolor:red%7C39.878071,-84.332209&sensor=true';
    //var googleApis_map_Url = 'http://maps.googleapis.com/maps/api/staticmap?size=300x300&maptype=hybrid&zoom=16&sensor=true&markers=size:mid%7Ccolor:red%7C' + latlng;
    var mapImg = '<img src="' + googleApis_map_Url + '" />';
    $("#map_canvas").html(mapImg);
}

function getResume(){
    getEducation();
    getWorkHistory();    
}

function getEducation(){  
    
    $("#education").kendoMobileListView({
        dataSource: EducationDataSource,
        template: kendo.template($("#educationTemplate").html())
    });
    $("#educationPh").kendoMobileListView({
        dataSource: EducationDataSource,
        template: kendo.template($("#educationTemplate").html())
    });
    EducationDataSource.read();
}

function getWorkHistory(){  
    
    $("#workHistory").kendoMobileListView({
        dataSource: WorkDataSource,
        template: kendo.template($("#workTemplate").html())
    });
    $("#workHistoryPh").kendoMobileListView({
        dataSource: WorkDataSource,
        template: kendo.template($("#workTemplate").html())
    });
    WorkDataSource.read();
}

function openEducationView(e){
    var id = $(e.button).data("id");
    var wi = EducationDataSource.get(id);
    kendo.bind($("#foo"), wi);
    $("#foo").kendoMobileModalView("open");
}

function closeEducationView(e){
    $("#foo").kendoMobileModalView("close");
}

function openHistoryView(e){
    var id = $(e.button).data("id");
    var wi = WorkDataSource.get(id);
    kendo.bind($("#foo2U2"), wi);
    $("#foo2U2").kendoMobileModalView("open");
}

function closeHistoryView(e){
    $("#foo2U2").kendoMobileModalView("close");
}

function sendEmail(){
    var status = $(".status");
    var validator = $("#myContactInfo").kendoValidator().data("kendoValidator");
    if(validator.validate()){
        var name = $("#firstname").val().trim() + " " + $("#lastname").val().trim();
        var email = $("#email").val().trim();
        var comments = $("#contactcomments").val().trim();
                
        Parse.initialize("GKUNuUaCJamW87YUZwFPpwWGBWlRRII0uLDuFYZv", "bajvKorzBS0t4ITR8ERFrm0Ka8nAbDtHrqSuAW5W");
        
        Parse.Cloud.run('sendMail', 
                { "toEmail": "charles.catron@gmail.com",
                  "fromEmail": email,
                  "subject": "My Resume Contact Form",
                  "comments": "Name: " + name + "\nComments: " + comments
                }, 
                {
                  success: function(result) {
                    app.navigate("myResume");
                    resetContact();
                  },
                  error: function(error) {
                    status.text("Oops! There was problem sending your message.").addClass("invalid");
                  }
                }
            );
        $("input#firstname").blur();
        $("input#lastname").blur();
        $("input#email").blur();
        $("input#contactcomments").blur();    
    }   
}

function resetContact(){
    var errmsgs = $('#myContactInfo').find('span.k-widget.k-tooltip.k-tooltip-validation.k-invalid-msg'); 
    errmsgs.hide();
    
    
    $("#firstname").val('');
    $("#lastname").val('');
    $("#email").val('');
    $("#contactcomments").val('');  
    $("input#firstname").blur();
    $("input#lastname").blur();
    $("input#email").blur();
    $("input#contactcomments").blur();  
}